library(performance)
library(randomForest)
library(dplyr)
library(clusterSim)
library(ggplot2)
library(hrbrthemes)
library(plotly)
library(tidyverse)
library(lme4)
library(olsrr)
library(AICcmodavg)
library(grid)
library(jtools)
library(huxtable)
library(flextable)
library(sjPlot)
library(sjmisc)
library(sjlabelled)
library(ggsci)



#loading dataset


setwd("C:/Users/Ashish Nambiar/Documents/Research/Navendu & Meghna/Data collection")
data<-read.csv("Final coorg data - Copy.csv")


#linear regression graphs

#dataset

p_data_mix<- data%>%
  filter(Species %in% c("Dim","Coch", "Mac","Hum","Lit"))%>%
  select(Species, Elevation,SSD,SLA_all,LDMC_all)%>% droplevels

#...............................................................................
#                                                                              .
#  Single linear regression                                                    .
#                                                                              .
#...............................................................................


#prepping data
data<-read.csv("Final coorg data - Copy.csv")
data

data_mix<- data%>%
  filter(Species %in% c("Dim","Coch", "Mac","Hum","Lit"))%>%
  select(Species,Elevation,SSD,SLA_all,LDMC_all)#%>% droplevels
data_mix
levels(data_mix$Species)

#scaling and log transformation
data_mix$SLA_all<-log(data_mix$SLA_all+1)
data_mix$LDMC_all<-log(data_mix$LDMC_all+1)
data_mix$SSD<-log(data_mix$SSD+1)
data_mix$SLA_all<-scale(data_mix$SLA_all)
data_mix$LDMC_all<-scale(data_mix$LDMC_all)
data_mix$SSD<-scale(data_mix$SSD)
data_mix$Elevation<-scale(data_mix$Elevation)

data_mix<-na.omit(data_mix)
colnames(data_mix)

#recode species names
data_mix<-data_mix %>%
  mutate(Species2 = recode(Species,
                           "Dim" = "DIMLO",
                           "Hum" = "HUBRU",
                           "Lit" = "LIFLO",
                           "Mac"="SYMMA",
                           "Coch" = "SYMCO"))

# Convert Species2 to a factor
data_mix$Species2 <- as.factor(data_mix$Species2)

# Reorder levels of Species2 to set "SYMCO" as the reference group
data_mix$Species2 <- relevel(data_mix$Species2, ref = "SYMCO")


#SLR model SLA


SLR.SLA <- lm(SLA_all ~ Elevation , data = data_mix)
summary(SLR.SLA)

SLR.SLA1 <- lm(SLA_all ~ Species2+ Elevation , data = data_mix)
summary(SLR.SLA1)


SLR.SLA2 <- lm(SLA_all ~ Species2* Elevation , data = data_mix)

summary(SLR.SLA2)

tab_model(SLR.SLA,SLR.SLA1, SLR.SLA2 )
anova(SLR.SLA,SLR.SLA1, SLR.SLA2)

#SLR model LDMC

SLR.LDMC <- lm(LDMC_all ~ Elevation , data = data_mix)
summary(SLR.LDMC)

SLR.LDMC1 <- lm(LDMC_all ~ Species2+ Elevation , data = data_mix)
summary(SLR.LDMC1)

SLR.LDMC2 <- lm(LDMC_all ~ Species2* Elevation , data = data_mix)
summary(SLR.LDMC2)

tab_model(SLR.LDMC,SLR.LDMC1, SLR.LDMC2) #combined coefficient table
anova(SLR.LDMC,SLR.LDMC1, SLR.LDMC2)

#SLR model SSD

SLR.SSD <- lm(SSD ~ Elevation , data = data_mix)
summary(SLR.SSD)

SLR.SSD1 <- lm(SSD ~ Species2+ Elevation , data = data_mix)
summary(SLR.SSD1)


SLR.SSD2 <- lm(SSD ~ Species*Elevation , data = data_mix)
summary(SLR.SSD2)

tab_model(SLR.SSD, SLR.SSD1, SLR.SSD2)#combined coefficient table

anova(SLR.SSD,SLR.SSD1)




#...............................................................................
#                                                                              .
#  PCA for all traits                                                          .
#                                                                              .
#...............................................................................

data_mix
mix_elev<-dplyr::select(data_mix,Elevation, Species)


mix_elev

pc_mix<- data_mix%>%
  select(SSD,SLA_all,LDMC_all)

pc_mix  

pc_mix1<-prcomp(pc_mix)
pc_mix1
summary(pc_mix1)

#retrieving output

pc_mix1<-as.data.frame(pc_mix1$x)
pc_mix1

#merging the dataframes

pc_data<-cbind(mix_elev,pc_mix1)
pc_data


#PCA model
PCA.lmodel<- lm(PC1~., data = pc_data)
PCA.lmodel                                             #PCA model, significant model is PC1
summary(PCA.lmodel)

#with only PC1

PC1.lmodel <- lm(PC1 ~Elevation+Species, data = pc_data)
PC1.lmodel                                              #PCA model
summary(PC1.lmodel)
plot(PC1.lmodel)

#with only PC2

PC2.lmodel <- lm(Elevation ~PC2, data = pc_data)
PC2.lmodel                                              #PCA model
summary(PC2.lmodel)
plot(PC2.lmodel)

#SLA
SLA_data_mix<- data%>%
  filter(Species %in% c("Dim","Coch", "Mac","Hum","Lit"))%>%
  select(Species, Elevation,SLA_all)%>% droplevels

SLA_data_mix<-SLA_data_mix %>%
  mutate(Species = recode(Species,
                          "Dim" = "Dimocarpus longan",
                          "Hum" = "Humboldtia brunonis",
                          "Lit" = "Litsea floribunda",
                          "Mac"="Symplocos macrophylla",
                          "Coch" = "Symplocos cochinchinensis"))


SLA_data_mix<-na.omit(SLA_data_mix)



x<-ggplot(SLA_data_mix, aes(x=Elevation, y=SLA_all)) +
  geom_point(aes(colour= Species, shape=Species), size = 5, alpha = .8) + 
  geom_smooth(method="lm", se= T, size = 1, aes(linetype = Species, group = Species)) +
  geom_smooth(method = 'lm',size = 1, colour = 'black', se = T) + scale_color_lancet()+
  scale_fill_lancet()+ylab("Specific leaf area (SLA)")+theme(axis.text=element_text(size=19),
                                                             axis.title=element_text(size=27,face="bold"),
                                                             legend.key.size = unit(2, 'cm'),
                                                             legend.text = element_text(size=22, face="italic"),
                                                             legend.title = element_text(size=24))+ guides(linetype=FALSE)+scale_shape_manual(values=c(15, 16, 17,18,19)) +                                           # Apply guides function
  guides(color = guide_legend(override.aes = list(size = 5)))+ 
  theme(axis.title.y = element_text(hjust=0.5,vjust=2,size=24, face=2))
x

#LDMC    

#data

LDMC_data_mix<- data%>%
  filter(Species %in% c("Dim","Coch", "Mac","Hum","Lit"))%>%
  select(Species, Elevation,LDMC_all)%>% droplevels

LDMC_data_mix<-LDMC_data_mix %>%
  mutate(Species = recode(Species,
                          "Dim" = "Dimocarpus longan",
                          "Hum" = "Humboldtia brunonis",
                          "Lit" = "Litsea floribunda",
                          "Mac"="Symplocos macrophylla",
                          "Coch" = "Symplocos cochinchinensis"))

LDMC_data_mix<-na.omit(LDMC_data_mix)

#graph

y<-ggplot(LDMC_data_mix, aes(x=Elevation, y=LDMC_all)) +
  geom_point(aes(colour= Species, shape=Species), size = 5, alpha = .8) + 
  geom_smooth(method="lm", se= T, size = 1, aes(linetype = Species, group = Species)) +
  geom_smooth(method = 'lm',size = 1, colour = 'black', se = T) + scale_color_lancet()+
  scale_fill_lancet() +ylab("Leaf dry matter content (LDMC)")+theme(axis.text=element_text(size=19),
                                                                    axis.title=element_text(size=27,face="bold"),
                                                                    legend.key.size = unit(2, 'cm'),
                                                                    legend.text = element_text(size=22, face="italic"),
                                                                    legend.title = element_text(size=24))+ guides(linetype=FALSE)+scale_shape_manual(values=c(15, 16, 17,18,19)) +                                           # Apply guides function
  guides(color = guide_legend(override.aes = list(size = 5)))+ 
  theme(axis.title.y = element_text(hjust=0.5,vjust=2,size=24, face=2))
y
y

#SSD


SSD_data_mix<- data%>%
  filter(Species %in% c("Dim","Coch", "Mac","Hum","Lit"))%>%
  select(Species, Elevation,SSD)%>% droplevels

SSD_data_mix<-SSD_data_mix %>%
  mutate(Species = recode(Species,
                          "Dim" = "Dimocarpus longan",
                          "Hum" = "Humboldtia brunonis",
                          "Lit" = "Litsea floribunda",
                          "Mac"="Symplocos macrophylla",
                          "Coch" = "Symplocos cochinchinensis"))

#graph

z<-ggplot(SSD_data_mix, aes(x=Elevation, y=SSD)) +
  geom_point(aes(shape= Species, colour=Species), size = 5, alpha = .8) +
  geom_smooth(method="lm", se= T, size = 1, aes(linetype = Species, group = Species)) +
  geom_smooth(method = 'lm',size = 1, colour = 'black', se = T) + scale_color_lancet()+
  scale_fill_lancet()+theme(axis.text=element_text(size=19),
                            axis.title=element_text(size=27,face="bold"),
                            legend.key.size = unit(2, 'cm'),
                            legend.text = element_text(size=22, face="italic"),
                            legend.title = element_text(size=24))+ guides(linetype=FALSE)+ 
  scale_shape_manual(values=c(15, 16, 17,18,19)) + ylab("Stem specific density (SSD)")+                                            # Apply guides function
  guides(color = guide_legend(override.aes = list(size = 5)))+ 
  theme(axis.title.y = element_text(hjust=0.5,vjust=2,size=24, face=2))
z


#Combined linear regression graphs 

ggarrange(x,y,z, common.legend = TRUE,  legend = "bottom", 
          labels = c("A", "B", "C"),
          ncol = 1)

#----------------------Boxplots for comparison of trait means--------------

#data for boxplot of SLA

p_data_mix1 <- p_data_mix %>%
  filter(Species != "mean") %>%  # exclude rows with "mean" in "Species" column
  mutate(Species = case_when(
    Species == "Dimocarpus longan" ~ "DIMLON",   # replace "Dimocarpus longan" with "DIMLON"
    Species == "Litsea floribunda" ~ "LITFLO",   # replace "Litsea floribunda" with "LITFLO"
    Species == "Humboldtia brunonis" ~ "HUMBRU",  # replace "Humboldtia brunonis" with "HUMBRU"
    Species == "Symplocos macrophylla" ~ "SYMMAC", # replace "Symplocos macrophylla" with "SYMMAC"
    Species == "Symplocos cochinchinensis" ~ "SYMCOC" # replace "Symplocos cochinchinensis" with "SYMCOC" 
  ))

##-----boxplot SLA 



box_SLA <- ggplot(p_data_mix1, aes(x = Species, y = SLA_all, fill = Species)) + 
  geom_boxplot(alpha = 0.3) +
  theme(panel.background = element_rect(fill = "white"),  # make background white
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        axis.title.y = element_text(size = 20, margin = margin(t = 0, r = 10, b = 0, l = 0, unit = "pt"), vjust = 0.5, angle = 90, face = "bold"),
        axis.title.x = element_text(size = 20, margin = margin(t = 10, r = 0, b = 0, l = 0, unit = "pt"), vjust = 0.5, face = "bold"),
        text = element_text(size = 16),
        plot.margin = margin(1,1,1,1, "cm"),  # add margin around the plot
        panel.border = element_rect(color = "black", fill = NA, size = 1)) +  # add border around the plot
  labs(y = "SLA") +
  scale_fill_brewer(palette = "Dark2") +
  theme(legend.position = "none")  # remove legend


box_SLA

#-----------------LDMC------------------


box_LDMC <- ggplot(p_data_mix1, aes(x = Species, y = LDMC_all, fill = Species)) + 
  geom_boxplot(alpha = 0.3) +
  theme(panel.background = element_rect(fill = "white"),  # make background white
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        axis.title.y = element_text(size = 20, margin = margin(t = 0, r = 10, b = 0, l = 0, unit = "pt"), vjust = 0.5, angle = 90, face = "bold"),
        axis.title.x = element_text(size = 20, margin = margin(t = 10, r = 0, b = 0, l = 0, unit = "pt"), vjust = 0.5, face = "bold"),
        text = element_text(size = 16),
        plot.margin = margin(1,1,1,1, "cm"),  # add margin around the plot
        panel.border = element_rect(color = "black", fill = NA, size = 1)) +  # add border around the plot
  labs(y = "LDMC") +
  scale_fill_brewer(palette = "Dark2") +
  theme(legend.position = "none")  # remove legend



box_LDMC


#------------------------SSD-------------------

#boxplot for SSD mean comparison
box_SSD<-ggplot(p_data_mix1, aes(x = Species, y = SSD, fill = Species)) + 
  geom_boxplot(alpha = 0.3) +
  theme(panel.background = element_rect(fill = "white"),  # make background white
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        axis.title.y = element_text(size = 20, margin = margin(t = 0, r = 10, b = 0, l = 0, unit = "pt"), vjust = 0.5, angle = 90, face = "bold"),
        axis.title.x = element_text(size = 20, margin = margin(t = 10, r = 0, b = 0, l = 0, unit = "pt"), vjust = 0.5, face = "bold"),
        text = element_text(size = 16),
        plot.margin = margin(1,1,1,1, "cm"),  # add margin around the plot
        panel.border = element_rect(color = "black", fill = NA, size = 1)) +  # add border around the plot
  labs(y = "SSD") +
  scale_fill_brewer(palette = "Dark2") +
  theme(legend.position = "none")  # remove legend


box_SSD

# Combine the box plots
ggarrange(box_SLA, box_LDMC, box_SSD, ncol = 2, nrow = 2, common.legend = TRUE, 
          legend = "bottom", widths = c(1.2, 1.2,1.2), heights = c(1.2, 1.2,1.2)) +
  theme(legend.position = "none")


xxx_box1("Single linear regression")


